# Instagram_clone

Instagram Clone will be using PHP laravel

## Installing Laravel

Laravel utilizes Composer to manage its dependencies. So, before using Laravel, make sure you have Composer installed on your machine.
Via Laravel Installer

First, download the Laravel installer using Composer:

```bash
composer global require laravel/installer
```
Make sure to place Composer's system-wide vendor bin directory in your $PATH so the laravel executable can be located by your system. This directory exists in different locations based on your operating system; however, some common locations include:

macOS: $HOME/.composer/vendor/bin
Windows: %USERPROFILE%\AppData\Roaming\Composer\vendor\bin
GNU / Linux Distributions: $HOME/.config/composer/vendor/bin or $HOME/.composer/vendor/bin

[Laravel Installation documentation site](https://laravel.com/docs/7.x#installing-laravel)


## Usage in Local Development Server

If you have PHP installed locally and you would like to use PHP's built-in development server to serve your application, you may use the serve Artisan command. This command will start a development server at http://localhost:8000:

```bash
php artisan serve
```

## Contributing

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)